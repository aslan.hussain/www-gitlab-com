---
layout: secure_and_protect_competitors
title: "GitLab vs Anchore"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

## Summary

Anchore is a company that offers security scanning for Docker containers, Docker container registries, and Kubernetes clusters. They offer an Open Source, Enterprise, and Federal version of their products. They leverage public vulnerability feeds to scan customers' environments for vulnerabilities and alert them so end users can take action.

## Comparison to GitLab

Although Anchore does software composition analysis well, they do very little beyond that narrow scope.  Comparatively, GitLab provides a superior experience for ALL types of security scanning - not only container scanning, but also SAST, DAST, Fuzz Testing, and others. This approach maximizes the kinds of vulnerabilities that can be detected while only incurring the maintenance costs of a single tool.

Anchore leverages publicly-available vulnerability feeds to identify their vulnerabilities.  GitLab does this as well; however, GitLab is also a CVE Numbering Authority, which means that security researchers can work directly with GitLab on any security issues they find.  GitLab's commitment to leveraging the latest vulnerability feeds is also publicly visible to customers at [advisories.gitlab.com](https://advisories.gitlab.com).

Finally, GitLab provides a superior experience for developers in viewing, correcting, and responding to vulnerabilities.  Because GitLab’s scanning capabilities are integrated with the rest of GitLab, the vulnerabilities appear as part of the developer’s regular workflow, inline within their MRs.  This visibility is critical to be able to effectively shift security left.  With Anchore, developers will need to look at an external tool to see the details about their vulnerabilities, making them much less likely to correct them before the code goes to production.

Anchore can be complementary to GitLab if users have already bought both. GitLab supports integration with tools that customers are already using and [plays well with others](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others).

## Software Composition Analysis (SCA)

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-protect strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Anchore</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Integrated security as part of DevOps workflow for all developers</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;High-quality container security by leveraging all the latest feeds for vulnerabilities</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Supports on-premise deployments including disconnected, offline, or air-gapped environments</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Security leadership by being a CVE Numbering Authority and a recognized [in the Gartner AST magic quadrant](https://about.gitlab.com/press/releases/2020-05-11-gitlab-positioned-niche-players-quadrant-2020-gartner-magic-quadrant-application-security-testing.html)</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End-to-end DevOps offering from SCM to CI to CD to Security and more</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Single-focused, purpose built container scanning product</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Can work with many CI/CD providers (e.g. GitHub, GitLab, BitBucket)</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pricing requires buying all of GitLab Ultimate, not just Container Scanning</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Narrow product offering only focused on one type of scanning</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It is difficult to justify the cost of maintaining an entire security tool when the tool addresses such limited scope (SCA only)</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-protect feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Anchore</b> |
| --- | :-: | :-: |
| Vulnerability Scanning | &#9989; | &#9989; |
| Secrets and Passwords | &#9989; | &#9989; |
| Open Source & Third Party Package Audit | &#9989; | &#9989; |
| Air-gapped Support | &#9989; | &#9989; |
| Security results shown to developers as part of their daily work | &#9989; | |

</div>
